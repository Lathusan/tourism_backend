package com.tourism.springboot.converters;

import java.util.ArrayList;
import java.util.List;

import com.tourism.springboot.dto.FeedbackDto;
import com.tourism.springboot.entities.Feedback;

public class FeedbackConverter {

	public static Feedback feedbackDtoToFeedback(FeedbackDto feedbackDto) {
		Feedback feedback = new Feedback();
		if (feedbackDto != null) {
			feedback.setId(feedbackDto.getId());
		//	feedback.setCustId(feedbackDto.getCustId());
			feedback.setCustName(feedbackDto.getCustName());
			feedback.setEmailId(feedbackDto.getEmailId());
			feedback.setDescription(feedbackDto.getDescription());

			return feedback;
		}
		return null;
	}

	public static List<FeedbackDto> feedbackToFeedbackDto(List<Feedback> feedbacklist) {
		List<FeedbackDto> listFeedbackDto = new ArrayList<>();
		if (feedbacklist != null) {
			for (Feedback feedback : feedbacklist) {
				FeedbackDto feedbackDto = new FeedbackDto();
				feedbackDto.setId(feedback.getId());
				feedbackDto.setCustName(feedback.getCustName());
				feedbackDto.setEmailId(feedback.getEmailId());
				feedbackDto.setDescription(feedback.getDescription());
				listFeedbackDto.add(feedbackDto);
			}
			return listFeedbackDto;
		}
		return null;
	}
	
}
