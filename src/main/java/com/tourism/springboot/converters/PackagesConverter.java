package com.tourism.springboot.converters;

import java.util.ArrayList;
import java.util.List;

import com.tourism.springboot.dto.PackagesDto;
import com.tourism.springboot.entities.Packages;

public class PackagesConverter {

	public static Packages packagesDtoToPackages(PackagesDto packagesDto) {
		Packages packages = new Packages();
		if (packagesDto != null) {
			packages.setId(packagesDto.getId());
			packages.setPackName(packagesDto.getPackName());
			packages.setPackDes(packagesDto.getPackDes());
			packages.setPackCost(packagesDto.getPackCost());
			packages.setPackPlan(packagesDto.getPackPlan());

			return packages;
		}
		return null;

	}

	public static List<PackagesDto> packagesToPackagesDto(List<Packages> packageslist) {
		List<PackagesDto> listPackagesDto = new ArrayList<>();
		if (packageslist != null) {
			for (Packages packages : packageslist) {
				PackagesDto packagesDto = new PackagesDto();
				packagesDto.setId(packages.getId());
				packagesDto.setPackName(packages.getPackName());
				packagesDto.setPackDes(packages.getPackDes());
				packagesDto.setPackCost(packages.getPackCost());
				packagesDto.setPackPlan(packages.getPackPlan());
				listPackagesDto.add(packagesDto);
			}
			return listPackagesDto;
		}
		return null;
	}

}
