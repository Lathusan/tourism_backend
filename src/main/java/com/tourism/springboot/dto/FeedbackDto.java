package com.tourism.springboot.dto;

public class FeedbackDto {

	private Long id;
//	private Long CustId;
	private String custName;
	private String emailId;
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*
	 * public Long getCustId() { return CustId; } public void setCustId(Long custId)
	 * { CustId = custId; }
	 */

}
